# m0lecon-mini-challs

Inside every challenge's folder you will find one or both of those folders:
 - src: the sources for the challenge, don't read them if you don't want spoilers. You can launch every challenge with docker-compose up
 - dist: the files distributed to the players during the competition. You can view those as they are necessary for the solution

 If one of those folders is missing it is not required for the challenge
